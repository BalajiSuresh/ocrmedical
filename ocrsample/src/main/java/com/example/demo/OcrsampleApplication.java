package com.example.demo;

import java.io.File;
import java.io.IOException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.example.demo.controller.OcrController;

@SpringBootApplication
public class OcrsampleApplication {

	public static void main(String[] args) throws IOException{
		new File(OcrController.uploadingDir).mkdirs();
		SpringApplication.run(OcrsampleApplication.class, args);
	}

}
