package com.example.demo.controller;

import org.apache.commons.io.FilenameUtils;
import org.json.JSONObject;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;

import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.Tesseract1;
import net.sourceforge.tess4j.TesseractException;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

@Controller
public class OcrController {
    public static final String uploadingDir = System.getProperty("user.dir") + "/uploadingDir/";

    @RequestMapping("/")
    public String uploading(Model model) {
        File file = new File(uploadingDir);
        model.addAttribute("files", file.listFiles());
        return "uploading";
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
	public ResponseEntity<String> OcrFetch(@RequestParam(name="file") MultipartFile file) throws Exception{
		String ext = FilenameUtils.getExtension(file.getOriginalFilename());
		if (!"png".equals(ext) && !"jpg".equals(ext)) {
			return ResponseEntity.badRequest().build();
		}
		String resultado = "";
		Gson gson = new Gson();
		
		try {
			BufferedImage img = ImageIO.read(file.getInputStream());
	        Tesseract tesseract = new Tesseract();
	        tesseract.setDatapath("C:\\Users\\Admin\\Downloads\\Tess4J-1.3-src\\Tess4J\\");
	        resultado = "";	     
            tesseract.setLanguage("eng");
			resultado = tesseract.doOCR(img);
			
		} catch (IOException e) {
			throw new Exception("Error");
		}
		return ResponseEntity.ok(gson.toJson(resultado));						
	}

}

